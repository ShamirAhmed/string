function problem1(data) {
    if (typeof data !== 'string' || data === undefined) {
        return 0;
    } else if (data.match(/[^$\d,-.]/g) !== null) {
        return 0;
    } else if (data.match(/-/g) !== null && data.match(/-/g).length > 1) {
        return 0;
    } else if (data.match(/-/g) !== null && data.charAt(0) !== '-') {
        return 0;
    } else if (data.match(/\$/g) === null) {
        return 0;
    } else if (data.match(/\$/g).length > 1) {
        return 0;
    } else if (data.charAt(0) !== '$' && data.match(/-/g) === null) {
        return 0;
    } else if (data.charAt(1) !== '$' && data.match(/-/g) !== null) {
        return 0;
    } else if (data.match(/\./g) !== null && data.match(/\./g).length > 1) {
        return 0;
    } else if (parseFloat(data.replace(/[-$,]/g, '')).toLocaleString() !== data.replace(/[-$]/g, '')) {
        if (parseFloat(data.replace(/[$,]/g, '')) === parseInt(data.replace(/[$,]/g, ''))) {
            return parseInt(data.replace(/[$,]/g, ''));
        }
        return 0
    } else {
        return parseFloat(data.replace(/[$,]/g, ''));
    }
}

module.exports = problem1;

