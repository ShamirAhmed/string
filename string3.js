function problem3(data) {
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    let date = new Date(data);
    return months[date.getMonth()];
}

module.exports = problem3;