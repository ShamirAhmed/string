function problem4(data) {
    if (data === undefined || typeof data !== 'object') {
        return '';
    } else {
        let fullName;
        if (data.middle_name !== undefined) {
            fullName = `${data.first_name} ${data.middle_name} ${data.last_name}`;
        } else {
            fullName = `${data.first_name} ${data.last_name}`;
        }
        let titleCase = fullName.replace(/\w+/g, (txt) => {
            return txt.charAt(0).toUpperCase() + txt.substring(1).toLowerCase();
        });

        return titleCase;
    }
}

module.exports = problem4;