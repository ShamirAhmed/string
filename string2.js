function problem2(data) {
    if (data === undefined || typeof data !== 'string') {
        return [];
    } else if (data.match(/[^\d.]/g)) {
        return [];
    } else {
        let ipAddress = data.split('.')
            .map((ip) => {
                return parseInt(ip);
            })
            .filter((ip) => {
                return ip <= 255;
            });

        if (data.split('.').length === ipAddress.length) {
            return ipAddress;
        } else {
            return [];
        }
    }
}

module.exports = problem2;