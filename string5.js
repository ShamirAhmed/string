function problem5(data) {
    if (data === undefined || typeof data !== 'object') {
        return '';
    } else if (data.length === 0) {
        return '';
    } else {
        return data.join(' ');
    }
}

module.exports = problem5;